import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.5"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.31"
	kotlin("plugin.spring") version "1.5.31"
	kotlin("plugin.jpa") version "1.5.31"
}

group = "ru.plinor"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.5.6")
	implementation("org.springframework.boot:spring-boot-starter-security:2.5.6")
	implementation("org.springframework.boot:spring-boot-starter-web:2.5.6")
	implementation("org.springframework:spring-web:5.3.12")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.0")
	implementation("javax.xml.bind:jaxb-api:2.4.0-b180830.0359")
	implementation("com.auth0:java-jwt:3.18.2")
	compileOnly("org.projectlombok:lombok:1.18.22")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	runtimeOnly("org.postgresql:postgresql:42.3.1")
	implementation("org.apache.commons:commons-lang3:3.12.0")
	implementation("com.google.guava:guava:31.0.1-jre")									//для хранения в cash данных пользователя
	implementation("com.sun.mail:javax.mail:1.6.2")										//для отправки email почты
	testImplementation("org.springframework.boot:spring-boot-starter-test:2.5.6")
	testImplementation("org.springframework.security:spring-security-test:5.5.1")
	testImplementation("org.hamcrest:hamcrest-library:2.2")
	testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
	testImplementation("org.mockito:mockito-core:4.0.0")
	testImplementation("com.h2database:h2:1.4.200")
	testImplementation("org.mockito:mockito-junit-jupiter:4.0.0")
	implementation("org.flywaydb:flyway-core:8.1.0")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
