create sequence hibernate_sequence start 1 increment 1;

create table plinor_user (
    user_id                      int8 not null,
    user_email                   varchar(255) not null,
    user_firstname               varchar(255),
    user_is_active               boolean,
    user_is_not_locked           boolean,
    user_join_date               timestamp,
    user_last_date_login         timestamp,
    user_last_date_login_display timestamp,
    user_lastname                varchar(255) not null,
    user_password                varchar(255) not null,
    user_profile_image_url       varchar(255),
    user_role                    varchar(255),
    user_user_id                 varchar(255),
    user_username                varchar(255),
    primary key (user_id)
);

create table plinor_user_authorities (
    plinor_user_user_id int8 not null,
    user_authorities    varchar(255)
);

create table role (
    role_id   int8 not null,
    role_name varchar(255),
    primary key (role_id)
);

alter table if exists plinor_user_authorities
    add constraint plinor_user_authorities_user_fk
    foreign key (plinor_user_user_id) references plinor_user;