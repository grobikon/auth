insert into plinor_user(user_id,
                        user_email,
                        user_firstname,
                        user_is_active,
                        user_is_not_locked,
                        user_join_date,
                        user_last_date_login,
                        user_last_date_login_display,
                        user_lastname,
                        user_password,
                        user_profile_image_url,
                        user_role,
                        user_user_id,
                        user_username)
    values (1,
            'grobikon@mail.ru',
            'admin',
            true,
            true,
            '2021-11-29',
            null,
            null,
            'admin',
            'XXX111zzz',
            null,
            'ROLE_SUPER_ADMIN',
            'db16a56592d94fecb2bbb1476ff190b2',
            'admin'
            );
insert into plinor_user_authorities(plinor_user_user_id, user_authorities) values (1, 'user:read');
insert into plinor_user_authorities(plinor_user_user_id, user_authorities) values (1, 'user:create');
insert into plinor_user_authorities(plinor_user_user_id, user_authorities) values (1, 'user:update');
insert into plinor_user_authorities(plinor_user_user_id, user_authorities) values (1, 'user:delete');