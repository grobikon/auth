create extension if not exists pgcrypto;

update plinor_user set user_password = crypt(user_password, gen_salt('bf', 8))