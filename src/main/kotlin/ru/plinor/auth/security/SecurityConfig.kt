package ru.plinor.auth.security

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.plinor.auth.constant.PUBLIC_URLS
import ru.plinor.auth.filter.*

/**
 * Конфигурация безопасности
 * @EnableWebSecurity - включаем web безопасность, требуется если вы хотите написать конфигурацию HttpSecurity, WebSecurity, AuthenticationManagerBuilder.
 * @EnableGlobalMethodSecurity - нужно включить глобальную безопасность метода,
 *                               включает AOP для конкретных методов или классов с помощью аннотаций @PreAuthorized, @PostAuthorize и т.п.
 *                               Включаем безопасность на уровне метода.
 * WebSecurityConfigurerAdapter - расширяем настройки адаптера web безопасности,
 * переопределим определенный методы и скажем
 * как мы хотим управлять пользователями и безопасностью приложения
 * @param jwtAuthorizationFilter - Фильтр авторизации по token JWT,
 *                                 будет перехватывать каждый запрос поступающий в приложение и обрабатывать только один раз
 * @param jwtAccessDeniedHandler - Попадаем в этот класс всякий раз когда пользователю запрещён доступ
 * @param jwtAuthorizationEntryPoint - Точка входа для авторизации пользователь
 *                                     не может предоставить аутентификацию и пробует получить доступ к приложению
 * @param userDetailsService - будем получать сведения о пользователе
 * @param bCryptPasswordEncoder - нужен для хэширования паролей
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig(
    private val jwtAuthorizationFilter: JwtAuthorizationFilter,
    private val jwtAccessDeniedHandler: JwtAccessDeniedHandler,
    private val jwtAuthorizationEntryPoint: JwtAuthorizationEntryPoint,
    @Qualifier("userDetailsService") private val userDetailsService: UserDetailsService,
    @Qualifier("BCrypt") private val bCryptPasswordEncoder: BCryptPasswordEncoder
): WebSecurityConfigurerAdapter() {

    /**
     * Говорим Spring, чтобы он воспользовался нашей службой информации о пользователях для управления аутентификацией.
     * Хотим, брать пользователей из БД.
     */
    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(userDetailsService)?.passwordEncoder(bCryptPasswordEncoder)
    }

    /**
     * Определяем что доступно всем и что доступно только авторизованным пользователям
     */
    override fun configure(http: HttpSecurity?) {
        //пользовательский фильтр аутентификации
        //val customAuthenticationFilter = CustomAuthenticationFilter(authenticationManagerBean())
        //customAuthenticationFilter.setFilterProcessesUrl("/api/login/**")
        http?.csrf()?.disable()                                                                                                             //Отключаем проверку межсайтовых запросов не будем её использовать
        http?.cors()                                                                                                                        //Добавим «совместное использование ресурсов между разными источниками», т.к. мы хотим получить общий доступ к ресурсам из разных источников
        http?.sessionManagement()?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)                                                   //Отключаем сессии
        http?.authorizeRequests()?.antMatchers(*PUBLIC_URLS)?.permitAll()                                                                   //Всем пользователям доступны эти url
        //http?.authorizeRequests()?.antMatchers(HttpMethod.GET, "/api/user/**")?.hasAnyAuthority("ROLE_USER")        //Разрешаем только пользователям путь до /api/user/**
        //http?.authorizeRequests()?.antMatchers(HttpMethod.POST, "/api/user/save/**")?.hasAnyAuthority("ROLE_ADMIN") //Разрешаем только администраторам сохранять пользователей
        http?.authorizeRequests()?.anyRequest()?.authenticated()                                                                            //Остальные url пути доступны только пользователям которые прошли аутентификацию
        http?.exceptionHandling()?.accessDeniedHandler(jwtAccessDeniedHandler)                                                              //Обрабатываем ошибки когда пользователю запрещён доступ
        http?.exceptionHandling()?.authenticationEntryPoint(jwtAuthorizationEntryPoint)                                                     //Обрабатываем ошибки когда не может предоставить аутентификацию и пробует получить доступ к приложению
        //http?.addFilter(customAuthenticationFilter)
        http?.addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter::class.java)                                     //Обрабатываем аутентификацию по логину и паролю
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }
}