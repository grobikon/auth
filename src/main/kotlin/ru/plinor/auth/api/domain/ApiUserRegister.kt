package ru.plinor.auth.api.domain

data class ApiUserRegister(
    val firstName: String,
    val lastName: String,
    val username: String,
    val email: String
)
