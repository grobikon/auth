package ru.plinor.auth.api.domain

import ru.plinor.auth.model.User

data class ApiJwtTokenWithUser(
    val jwtToken: String,
    val refreshJwtToken: String,
    val user: User
)