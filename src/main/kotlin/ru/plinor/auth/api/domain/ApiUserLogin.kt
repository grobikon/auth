package ru.plinor.auth.api.domain

data class ApiUserLogin(
    val username: String,
    val password: String
)