package ru.plinor.auth.api

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.MediaType.IMAGE_JPEG_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import ru.plinor.auth.api.domain.ApiJwtTokenWithUser
import ru.plinor.auth.api.domain.ApiUserLogin
import ru.plinor.auth.api.domain.ApiUserRegister
import ru.plinor.auth.constant.*
import ru.plinor.auth.model.HttpResponse
import ru.plinor.auth.model.Role
import ru.plinor.auth.model.User
import ru.plinor.auth.model.UserPrincipal
import ru.plinor.auth.exception.ExceptionHandling
import ru.plinor.auth.service.ServiceUser
import ru.plinor.auth.util.JWTTokenProvider
import java.io.ByteArrayOutputStream
import java.net.URI
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * Будем обрабатывать всем запросы от пользователя
 * ExceptionHandling - класс, который будет перехватывать ошибки spring и обрабатывать их
 * @param userService - отправляем запросы к БД
 * @param authenticationManager - менеджер spring, который проверяет подлинность пользователя
 * @param jwtTokenProvider - общий класс для создания JWT токена pass test lwYbvIFsPy
 */
@RestController
@RequestMapping(path = ["/","/api"])
class UserResource(
    val userService: ServiceUser,
    val authenticationManager: AuthenticationManager,
    val jwtTokenProvider: JWTTokenProvider
): ExceptionHandling() {

    @PostMapping("/login")
    fun login(@RequestBody user: ApiUserLogin): ResponseEntity<ApiJwtTokenWithUser> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        //Проверяем аутентификацию пользователя
        authenticate(user.username, user.password)
        //Получаем пользователя
        val loginUser = userService.findUserByUsername(username = user.username)!!
        //Получаем данные userPrincipal для создания JWT токена в заголовках
        val userPrincipal = UserPrincipal(loginUser)
        val jwtHeader = getJwtHeader(userPrincipal)
        val apiJwtTokenWithUser = ApiJwtTokenWithUser(
            jwtToken = jwtTokenProvider.generateJwtToken(userPrincipal = userPrincipal),
            refreshJwtToken = jwtTokenProvider.generateJwtToken(userPrincipal = userPrincipal),
            user = loginUser
        )
        return ResponseEntity.ok().headers(jwtHeader).body(apiJwtTokenWithUser)
    }

    @PostMapping("/user/register")
    fun register(@RequestBody user: ApiUserRegister): ResponseEntity<User> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        val newUser = userService.registerUser(user.firstName, user.lastName, user.username, user.email)
        return ResponseEntity.ok().body(newUser)
    }

    /**
     * Создание нового пользователя
     * value = "profileImage" - значит другой формат параметра
     * required = false - не обязательный параметр
     * Можно будет добавить пользователя без фото
     */
    @PostMapping("/user/add")
    fun addNewUser(@RequestParam("firstName") firstName: String,
                   @RequestParam("lastName") lastName: String,
                   @RequestParam("username") username: String,
                   @RequestParam("email") email: String,
                   @RequestParam("role") role: String,
                   @RequestParam("isActive") isActive: String,
                   @RequestParam("isNotLocked") isNotLocked: String,
                   @RequestParam(value = "profileImage", required = false)
                   profileImage: MultipartFile?): ResponseEntity<User> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        val newUser = userService.addNewUser(
            firstName = firstName,
            lastName = lastName,
            username = username,
            email = email,
            role = role,
            isActive = isActive.toBoolean(),
            isNotLocket = isNotLocked.toBoolean(),
            profileImage = profileImage)
        return ResponseEntity.ok().body(newUser)
    }

    /**
     * Создание нового пользователя
     * value = "profileImage" - значит другой формат параметра
     * required = false - не обязательный параметр
     * Можно будет добавить пользователя без фото
     */
    @PostMapping("/user/update")
    fun updateUser(@RequestParam("currentUsername") currentUsername: String,
                   @RequestParam("firstName") newFirstName: String,
                   @RequestParam("lastName") newLastName: String,
                   @RequestParam("username") newUsername: String,
                   @RequestParam("email") newEmail: String,
                   @RequestParam("role") newRole: String,
                   @RequestParam("isActive") isActive: String,
                   @RequestParam("isNotLocked") isNotLocked: String,
                   @RequestParam(value = "profileImage", required = false)
                   profileImage: MultipartFile?): ResponseEntity<User> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        val updateUser = userService.updateUser(
            currentUsername = currentUsername,
            newFirstName = newFirstName,
            newLastName = newLastName,
            newUsername = newUsername,
            newEmail = newEmail,
            newRole = newRole,
            isActive = isActive.toBoolean(),
            isNotLocket = isNotLocked.toBoolean(),
            profileImage = profileImage)
        return ResponseEntity.ok().body(updateUser)
    }

    @GetMapping("/user/find/{username}")
    fun getUser(@PathVariable username: String): ResponseEntity<User> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        return ResponseEntity.ok().body(userService.findUserByUsername(username))
    }

    @GetMapping("/user/list-users")
    fun getAllUsers(): ResponseEntity<List<User>> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        return ResponseEntity.ok().body(userService.getUsers())
    }

    @GetMapping("/user/reset-password/{email}")
    fun resetPassword(@PathVariable email: String): ResponseEntity<HttpResponse> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        userService.resetPassword(email)
        return response(HttpStatus.OK, EMAIL_SEND + email)
    }

    /**
     * Удаляем пользователя из БД по id
     * @PreAuthorize("hasAnyAuthority('user:delete')") - безопасность на уровне метода в настройках безопасности
     *                                                   должна быть прописана аннотация @EnableGlobalMethodSecurity(prePostEnabled = true)
     */
    @DeleteMapping("/user/delete/{userId}")
    @PreAuthorize("hasAnyAuthority('user:delete')")
    fun deleteUser(@PathVariable("userId") userId: String): ResponseEntity<HttpResponse> {
        userService.deleteUser(userId)
        return response(HttpStatus.OK, USER_DELETED_SUCCESSFULLY)
    }

    /**
     * Обновляем фото пользователя
     * value = "profileImage" - значит другой формат параметра
     * required = false - не обязательный параметр
     * Можно будет добавить пользователя без фото
     */
    @PostMapping("/user/updateProfileImage")
    fun updateProfileImage(@RequestParam("username") username: String,
                   @RequestParam("profileImage") profileImage: MultipartFile): ResponseEntity<User> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        val user = userService.updateProfileImage(username = username, profileImage = profileImage)
        return ResponseEntity.ok().body(user)
    }

    /**
     * Переносим картинку пользователя в файловую систему
     * produces - тип изображения, т.е. к этому запросу нужно перекрепить изображение
     */
    @GetMapping(path = ["/user/image/{username}/{fileName}"], produces = [IMAGE_JPEG_VALUE])
    fun getProfileImage(@PathVariable("username") username: String, @PathVariable("fileName") fileName: String): ByteArray {
        //Читаем файл по байтам в том месте где он лежит и отправляем его пользователю в браузер
        return Files.readAllBytes(Paths.get(USER_FOLDER + username + FORWARD_SLASH + fileName))
    }

    /**
     * Переносим картинку пользователя в файловую систему
     * produces - тип изображения, т.е. к этому запросу нужно перекрепить изображение
     */
    @GetMapping(path = ["/user/image/profile/{username}"], produces = [IMAGE_JPEG_VALUE])
    fun getTempProfileImage(@PathVariable("username") username: String): ByteArray {
        //Загружаем default image для пользователя из интернета
        val url = URL(TEMP_PROFILE_IMAGE_BASE_URL + username + TEMP_PROFILE_IMAGE_PARAM)
        //Будем хранить все данные поступаемые из URL
        val byteArrayOutputStream = ByteArrayOutputStream()
        url.openStream().use { inputStream ->
            var bytesRead: Int
            val chunk = ByteArray(1024)
            while (inputStream.read(chunk).also { bytesRead = it } > 0) {
                byteArrayOutputStream.write(chunk, 0, bytesRead)
            }
        }
        return byteArrayOutputStream.toByteArray()
    }

    /**
     * Общий метод для отправки ответа пользователю на запрос
     * @param httpStatus - статус ответа на запрос пример 200
     * @param message - сообщение ответа на запрос
     */
    private fun response(httpStatus: HttpStatus, message: String): ResponseEntity<HttpResponse> {
        val httpResponse = HttpResponse(
            httpStatusCode = httpStatus.value(),
            httpStatus = httpStatus,
            reasonPhrase = httpStatus.reasonPhrase,
            message = message
        )
        return ResponseEntity<HttpResponse>(httpResponse, httpStatus)
    }

    @GetMapping("/users")
    fun getUsers(): ResponseEntity<List<User>> {
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем всех пользователей
        return ResponseEntity.ok().body(userService.getUsers())
    }

    /**
     * Сохраняем пользователя
     * @param user - @RequestBody в теле запроса получаем пользователя для сохранения
     */
    @PostMapping("/user/save")
    fun saveUser(@RequestBody user: User): ResponseEntity<User>{
        //body - в теле возвращаем сохраненного пользователя
        //created() - 201 создан, что-то было создано на сервере
        val uri: URI = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user/save").toUriString())
        return ResponseEntity.created(uri).body(userService.saveUser(user))
    }

    /**
     * Сохраняем роль
     * @param role - @RequestBody в теле запроса получаем роль для сохранения
     */
    @PostMapping("/role/save")
    fun saveRole(@RequestBody role: Role): ResponseEntity<Role>{
        //body - в теле возвращаем сохраненного пользователя
        //created() - 201 создан, что-то было создано на сервере
        val uri: URI = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString())
        return ResponseEntity.created(uri).body(userService.saveRole(role))
    }

    /**
     * Сохраняем роль для пользователя
     * @param form - класс который содержит имя пользователя и имя роли
     */
    @PostMapping("/role/addtouser")
    fun addRoleTOUser(@RequestBody form: RoleToUserForm): ResponseEntity<Any>{
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем сохраненного пользователя
        userService.addRoleToUser(form.username, form.roleName)
        return ResponseEntity.ok().build()
    }

    /**
     * Обновляем токен JWT, если основной токен истёк
     * @param request -
     * @param response -
     */
    @PostMapping("/token/refresh")
    fun refreshToken(request: HttpServletRequest,
                     response: HttpServletResponse){
        //ok - 200 ответ пользователю, что все прошло хорошо
        //body - в теле возвращаем сохраненного пользователя
        //Проверяем наличие токена в запросе в заголовке
        val authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
        when(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            true -> {
                try {
                    //Получаем переданный токен из заголовка
                    val refreshTokenJwt = authorizationHeader.substring("Bearer ".length)
                    val algorithm = Algorithm.HMAC256("secret".toByteArray())       //secret нужно сохранить в безопасном месте и зашифрованном виде, и доступ к нему иметь из служебного класса
                    //С помощью секретного ключа расшифровываем токен
                    val verifier: JWTVerifier = JWT.require(algorithm).build()
                    val decoderJWT: DecodedJWT = verifier.verify(refreshTokenJwt)
                    //Получаем имя пользователя
                    val username = decoderJWT.subject
                    val user = userService.getUser(username)?:throw RuntimeException("Обновлении токена: Ошибка в БД нет такого токена.")
                    //Создаём токен
                    //val roles = null//user.roles?.stream()?.map(Role::name)?.collect(Collectors.toList())
                    val accessToken = JWT.create()
                        .withSubject(user.username)                                                //идентификатор пользователя, уникальное значение пользователя по нему будем искать пользователя
                        .withExpiresAt(Date(System.currentTimeMillis() + 5 * 60 * 1000))     //время жизни токена 1 минут когда он будет недействителен
                        .withIssuer(request.requestURL.toString())                                 //автор этого токена Url адрес приложения клиента, которое запрашивает токен
                        //.withClaim("roles", roles)                                           //Список правил ролей пользователя
                        .sign(algorithm)
                    //Создаём токен обновления
                    val refreshToken = JWT.create()
                        .withSubject(user.username)                                                 //идентификатор пользователя, уникальное значение пользователя по нему будем искать пользователя
                        .withExpiresAt(Date(System.currentTimeMillis() + 30 * 60 * 1000))     //время жизни токена 30 минут когда он будет недействителен
                        .withIssuer(request.requestURL.toString())                                 //автор этого токена Url адрес приложения клиента, которое запрашивает токен
                        .sign(algorithm)                                                            //подписываем этот токен алгоритмом
                    //Отправляем токены в виде json
                    val tokens: MutableMap<String, String> = HashMap()
                    tokens["access_token"] = accessToken
                    tokens["refresh_token"] = refreshToken
                    response.contentType =
                        MediaType.APPLICATION_JSON_VALUE                                  //Возвращаем токены в теле ответа в виде json
                    ObjectMapper().writeValue(response.outputStream, tokens)
                }catch (e: Exception) {
                    response.setHeader("error", e.message)
                    response.status = HttpStatus.FORBIDDEN.value()
                    val error: MutableMap<String, String> = HashMap()
                    error["error_message"] = e.message.toString()
                    response.contentType = MediaType.APPLICATION_JSON_VALUE                                  //Возвращаем токены в теле ответа в виде json
                    ObjectMapper().writeValue(response.outputStream, error)
                }
            }
            false -> throw RuntimeException("Ошибка в обновлении токена.")
        }
    }

    /**
     * Проверяем аутентификацию пользователя.
     * Если передадут неправильный логин и пароль или если их учётная запись заблокирована или отключена.
     * Мы покажем сообщение с ошибкой и дальнейшая работа метода в котором вызвали этот метод прекратиться.
     * @param username - логин пользователя который будем искать в БД
     * @param password - пароль пользователя который будем проверять
     */
    private fun authenticate(username: String, password: String) {
        //проверяем подлинность пользователя по логину и паролю
        //если не верный логин и пароль отправит ошибку
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
    }

    /**
     * Добавляем сгенерированный JWT токен в заголовок ответа запроса
     * @param userPrincipal - нужен для создания JWT токена, содержит информацию о пользователи, в виде spring со всей информацией, ролями и права
     */
    private fun getJwtHeader(userPrincipal: UserPrincipal): HttpHeaders {
        val httpHeaders = HttpHeaders()
        httpHeaders.add(JWT_TOKEN_HEADER, jwtTokenProvider.generateJwtToken(userPrincipal = userPrincipal))
        return httpHeaders
    }
}

data class RoleToUserForm(val username: String, val roleName: String)