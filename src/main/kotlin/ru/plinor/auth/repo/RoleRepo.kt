package ru.plinor.auth.repo

import org.springframework.data.jpa.repository.JpaRepository
import ru.plinor.auth.model.Role

interface RoleRepo: JpaRepository<Role, Long> {
    fun findByName(name: String): Role?
}