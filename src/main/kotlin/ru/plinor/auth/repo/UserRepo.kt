package ru.plinor.auth.repo

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.plinor.auth.model.User

/**
 * Класс для управления таблицей Пользователи в БД, Long первичный ключ
 * В JpaRepository передаем класс, которым хотим управлять User и тип первичного ключа Long
 */
@Repository
interface UserRepo : JpaRepository<User, Long> {

    /**
     * Поиск пользователя по имени пользователя
     */
    fun findByUsername(name: String?): User?

    /**
     * Поиск пользователя по email пользователя
     */
    fun findByEmail(email: String?): User?

    /**
     * Поиск пользователя по email пользователя
     */
    fun findByUserId(userId: String?): User?

    /**
     * Поиск всех пользователей, с возможностью пагинации
     */
    //@Query("")
    //fun getUsers(): List<User>

    /**
     * Поиск всех пользователей, с возможностью пагинации
     */
    @Query(
        value = "SELECT * FROM plinor_user ORDER BY user_firstname, user_lasttname",
        nativeQuery = true
    )
    fun getUsers(): List<User>

    /**
     * Удаляем пользователя по userId
     */
    @Query(
        value = "DELETE FROM plinor_user WHERE user_user_id = :userId CASCADE",
        nativeQuery = true
    )
    fun deleteByUserId(@Param("userId") userId: String)
}