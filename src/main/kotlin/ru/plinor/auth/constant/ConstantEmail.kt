package ru.plinor.auth.constant

/**
 * Константы, чтобы отправлять письма пользователям
 */
const val SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtps"               //протокол передачи почты
const val USERNAME = "grobikontest2@gmail.com"                   //email адрес с которого будут отправлены письма
const val PASSWORD = "googleTest2"                               //пароль от почты отправителя
const val FROM_EMAIL = "support@plinor.ru"
const val CC_EMAIL = ""
const val EMAIL_SUBJECT = "ООО \"РЦ \"ПЛИНОР\" - Новый пароль."
const val GMAIL_SMTP_SERVER = "smtp.gmail.com"                  //почтовый сервер
const val SMTP_HOST = "mail.smtp.host"
const val SMTP_AUTH = "mail.smtp.auth"
const val SMTP_PORT = "mail.smtp.port"
const val DEFAULT_PORT = 465
const val SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable"
const val SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required"