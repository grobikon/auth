package ru.plinor.auth.constant

/**
 * Храним текстовые константы
 */
const val EXPIRATION_TIME: Long = 432_000_000                                   //5 days в миллисекундах срок действия JWT
const val TOKEN_PREFIX: String = "Bearer "                                      //Перед JWT всегда идет это слово Bearer JWT
const val JWT_TOKEN_HEADER: String = "Jwt-Token"                                //Пользовательский заголовок с JWT
const val TOKEN_CANNOT_BE_VERIFIED: String = "Token cannot be verified"         //Пользовательский заголовок с JWT
const val GET_ARRAYS_LLC: String = "Get Arrays, PLINOR"                         //
const val GET_ARRAYS_ADMINISTRATION: String = "User Management Portal"          //Пользовательский заголовок с JWT
const val AUTHORITIES: String = "Authorities"                                   //Пользовательский заголовок с JWT
const val FORBIDDEN_MESSAGE: String = "Нет доступа для этого запроса"     //Нужно получить доступ к этой странице
const val ACCESS_DENIED_MESSAGE: String = "Нет разрешения для этого запроса"
const val OPTION_HTTP_METHOD: String = "OPTIONS"
const val EMAIL_SEND = "Письмо с новым паролем, отправленное на email: "
const val USER_DELETED_SUCCESSFULLY = "Пользователь успешно удален."
//общедоступные URL все будут иметь к ним доступ
val PUBLIC_URLS = arrayOf(
    "/api/user/register",
    "/api/login",
    "/api/user/token/refresh/**",
    "/api/user/image/**" )
