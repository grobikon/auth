package ru.plinor.auth.constant

/**
 * Константы для работы с фалами
 */
const val USER_IMAGE_PATH = "/api/user/image/"                                  //путь к изображению пользователя
const val JPG_EXTENSION = "jpg"                                                 //тип файла картинки
val USER_FOLDER = System.getProperty("user.home") + "/plinorimageuser/user/"
const val DIRECTORY_CREATED = "Создан путь до: "
const val DEFAULT_PATH_USER_IMAGE = "/api/user/image/profile/"
const val FILE_SAVED_IN_FILE_SYSTEM = "Сохраненный файл в файловой системе по имени: "
const val DOT = "."
const val FORWARD_SLASH = "/"
const val NOT_AN_IMAGE_FILE = " не является файлом изображения. Пожалуйста, загрузите файл изображения"
const val TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/"
const val TEMP_PROFILE_IMAGE_PARAM = "?set=set5"