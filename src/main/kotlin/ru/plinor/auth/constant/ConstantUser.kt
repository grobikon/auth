package ru.plinor.auth.constant

/**
 * Константы для UserServiceImpl. Будем использовать для создания сообщений
 */
const val ALREADY_EXISTS_USERNAME = "Имя пользователя уже занято: "
const val ALREADY_EXISTS_EMAIL = "Email адрес уже занят: "
const val NO_USER_FOUND_BY_USERNAME = "Не найдено ни одного пользователя по имени: "
const val NO_USER_FOUND_BY_EMAIL = "Не найдено ни одного пользователя с таким email: "
const val NO_USER_FOUND_BY_USER_ID = "Не найдено ни одного пользователя с таким идентификатором пользователя: "