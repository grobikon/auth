package ru.plinor.auth.constant

/**
 * Константы, определяем какие есть права у пользователя
 */
val AUTHORITIES_USER = arrayOf("user:read")
val AUTHORITIES_MANAGER = arrayOf("user:read", "user:update")
val AUTHORITIES_ADMIN = arrayOf("user:read", "user:create", "user:update")
val AUTHORITIES_SUPER_ADMIN = arrayOf("user:read", "user:create", "user:update", "user:delete")