package ru.plinor.auth.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.Hibernate
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 * Таблица пользователя в БД
 * FetchType.EAGER - загружает роли пользователя
 * @Serializable - чтобы сохранять данные в БД через поток
 */
@Entity(name = User.TABLE)
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "${PREFIX}id", nullable = false, updatable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var id: Long? = null,                                                                       //Id Для базы данных, его нельзя обновлять и он не может быть равен null
    @Column(name = "${PREFIX}user_id") var userId: String? = null,                              //Реальный идентификатор пользователя
    @Column(name = "${PREFIX}firstname") var firstName: String,                                 //Имя пользователя
    @Column(name = "${PREFIX}lastname") var lastName: String,                                   //Фамилия пользователя
    @Column(name = "${PREFIX}username") var username: String,                                   //Может быть эл. почта
    @Column(name = "${PREFIX}password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var password: String,                                                                       //Пароль пользователя
    @Column(name = "${PREFIX}email") var email: String,                                         //Email пользователя
    @Column(name = "${PREFIX}profile_image_url") var profileImageUrl : String? = null,          //url - ссылка для загрузки картинки профиля пользователя
    @Column(name = "${PREFIX}last_date_login") var lastDateLogin : Date? = null,                //дата последнего входа в систему
    @Column(name = "${PREFIX}last_date_login_display") var lastDateLoginDisplay : Date? = null, //дата последнего входа в систему для отображения
    @Column(name = "${PREFIX}join_date") var joinDate : Date? = null,                           //дата создания учётной записи
    @Column(name = "${PREFIX}role") var role: String? = null,                                   //Роль пользователя ROLE_USER( read, edit ), ROLE_ADMIN ( delete, update, create)
    @ElementCollection
    @Column(name = "${PREFIX}authorities") var authorities: MutableList<String>,                //Список прав пользователя ( delete, update, create)
    @Column(name = "${PREFIX}is_active") var isActive : Boolean,                                //Активный пользователь или нет
    @Column(name = "${PREFIX}is_not_locked") var isNotLocked : Boolean,                         //Не заблокирован пользователь или нет
): Serializable {
    companion object{
        const val TABLE = "plinor_user"
        const val PREFIX = "user_"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as User

        return id != null && id == other.id
    }

    override fun hashCode(): Int = 0

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , userId = $userId , firstname = $firstName , lastname = $lastName , username = $username , password = $password , email = $email , profileImageUrl = $profileImageUrl , lastDateLogin = $lastDateLogin , lastDateLoginDisplay = $lastDateLoginDisplay , joinDate = $joinDate , role = $role , authorities = $authorities , isActive = $isActive , isNotLocked = $isNotLocked )"
    }
}
