package ru.plinor.auth.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors

class UserPrincipal(
    private val user: User
): UserDetails {

    /**
     * Полномочия роли пользователя пример (delete, update, create)
     * Что пользователь сможет делать
     */
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return this.user.authorities.stream().map(::SimpleGrantedAuthority).collect(Collectors.toList())
    }

    override fun getPassword(): String {
        return this.user.password
    }

    override fun getUsername(): String {
        return this.user.username
    }

    /**
     * У аккаунта еще не истек срок действия
     */
    override fun isAccountNonExpired(): Boolean {
        return true
    }

    /**
     * Проверяем заблокирован пользователь или нет
     */
    override fun isAccountNonLocked(): Boolean {
        return this.user.isNotLocked
    }

    /**
     * Срок действия учётных данных для пользователя
     */
    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    /**
     * Проверяем активный пользователь или нет
     */
    override fun isEnabled(): Boolean {
        return this.user.isActive
    }
}