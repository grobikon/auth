package ru.plinor.auth.model

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.http.HttpStatus
import java.util.*

/**
 * Общий ответ который мы будем отправлять пользователю при любом запросе к серверу
 *
 * {
 * "timeStamp" : "MM-dd-yyyy hh:mm:ss"
 *  "httpStatusCode": "200"
 *  "httpStatus": HttpStatus.OK
 *  "reason": "ок"
 *  "message": "запрос был успешный"
 * }
 */
data class HttpResponse(
    @get:JsonFormat(
        shape = JsonFormat.Shape.STRING,
        pattern = "dd.MM.yyyy hh:mm:ss",
        timezone="Europe/Moscow")
    val timeStamp: Date = Date(), //дата и время когда отправляли запрос
    var httpStatusCode: Int,     //Код статуса ответа сервера 200, 201, 400, 500 и т.д.
    var httpStatus: HttpStatus,  //EnumClass для получения текста статуса ответа
    var reasonPhrase: String,    //причина
    var message: String          //информация
)