package ru.plinor.auth.model

import javax.persistence.*

/**
 * Таблица ролей пользователя в БД
 */
@Entity(name = Role.TABLE)
class Role(
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "${PREFIX}id") var id: Long? = null,
    @Column(name = "${PREFIX}name") var name: String,         //Название роли пользователя
){
    companion object{
        const val TABLE = "role"
        const val PREFIX = "role_"
    }
}
