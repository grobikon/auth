package ru.plinor.auth.myEnum

import ru.plinor.auth.constant.AUTHORITIES_ADMIN
import ru.plinor.auth.constant.AUTHORITIES_MANAGER
import ru.plinor.auth.constant.AUTHORITIES_SUPER_ADMIN
import ru.plinor.auth.constant.AUTHORITIES_USER

/**
 * Определяем правила, которые будем предоставлять пользователю
 */
enum class EnumRole(val authorities: Array<String>) {
    ROLE_USER(AUTHORITIES_USER),
    ROLE_MANAGER(AUTHORITIES_MANAGER),
    ROLE_ADMIN(AUTHORITIES_ADMIN),
    ROLE_SUPER_ADMIN(AUTHORITIES_SUPER_ADMIN);

    companion object {

        @JvmStatic
        fun getRoleEnumName(role: String): EnumRole = valueOf(role.uppercase())
    }
}