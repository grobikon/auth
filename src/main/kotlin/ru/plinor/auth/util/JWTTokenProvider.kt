package ru.plinor.auth.util

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import ru.plinor.auth.constant.*
import ru.plinor.auth.model.UserPrincipal
import java.util.*
import java.util.stream.Collectors
import javax.servlet.http.HttpServletRequest

/**
 * Общий класс для создания JWT токена
 */
@Component
class JWTTokenProvider(
    @Value("\${jwt.secret}") private val secret: String //Нужно хранить в безопасном месте
) {

    /**
     * Генерируем токен JWT для пользователя
     * @param userPrincipal - при попытке генерации токена проверяем учётные данные в БД,
     * если учетные данные впорядке мы можем создать JWT
     */
    fun generateJwtToken(userPrincipal: UserPrincipal): String {
        val claims: List<String> = getClaimsFromUser(userPrincipal)
        val algorithm = Algorithm.HMAC512(secret.toByteArray())       //secret нужно сохранить в безопасном месте и зашифрованном виде, и доступ к нему иметь из служебного класса
        return JWT.create()
            .withIssuer(GET_ARRAYS_LLC)                             //автор этого токена Url адрес приложения клиента, которое запрашивает токен
            .withAudience(GET_ARRAYS_ADMINISTRATION)
            .withIssuedAt(Date())
            .withSubject(userPrincipal.username)                    //идентификатор пользователя, уникальное значение пользователя по нему будем искать пользователя
            .withClaim(AUTHORITIES, claims)                         //Список правил ролей пользователя
            .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))  //время жизни токена 10 минут когда он будет недействителен
            .sign(algorithm)
    }

    /**
     * Получаем список прав доступа для пользователя из токена JWT.
     * Представляет права, предоставленные объекту аутентификации.
     * @param tokenJWT - который приходит к нам от пользователя вместе с запросом
     */
    fun getAuthorities(tokenJWT: String): List<GrantedAuthority> {
        val claims: List<String> = getClaimsFromToken(tokenJWT)
        return claims.stream().map(::SimpleGrantedAuthority).collect(Collectors.toList())
    }

    /**
     * Выполняем аутентификацию пользователя, после проверки токена JWT
     * Попытка аутентификации, когда пользователь пытается получить доступ к системе.
     * Сообщаем Spring что этот пользователь аутентифицирован
     * @param username - имя пользователя, которое мы извлекли из JWT токена
     * @param authorities - права пользователя в spring, которое мы извлекли из JWT токена
     * @param request - http запрос, который делает пользователь GET, POST и т.д.
     */
    fun getAuthentication(username: String,
                          authorities: List<GrantedAuthority>,
                          request: HttpServletRequest): Authentication {
        //маркер аутентификации имени пользователя без пароля, так как мы уже проверили токен JWT
        //создаст информацию об этом пользователи в контексте безопасности spring
        val authenticationToken = UsernamePasswordAuthenticationToken(username, null, authorities)
        authenticationToken.details = WebAuthenticationDetailsSource().buildDetails(request)
        return authenticationToken
    }

    /**
     * Проверяем JWT токен на правильность или нет
     * @param username - имя пользователя, будем проверять чтобы оно было не равно null
     * @param tokenJWT - JWT токен который будем проверять, истёк срок годности или нет
     */
    fun isTokenJWYValid(username: String, tokenJWT: String): Boolean =
        StringUtils.isNotEmpty(username) && !isTokenJWTExpired(tokenJWT)

    /**
     * Получаем объект, который есть внутри токена JWT в виде строки, например username
     * @param tokenJWT - JWT токен
     */
    fun getSubject(tokenJWT: String): String = getJWTVerifier().verify(tokenJWT).subject

    /**
     * Проверяем дату действия token JWT, истек срок действия true, нет false
     * @param tokenJWT - токен у которого будем проверять дату окончания действия токена
     */
    private fun isTokenJWTExpired(tokenJWT: String): Boolean = getJWTVerifier().verify(tokenJWT).expiresAt.before(Date())

    /**
     * Определяем список прав доступа для пользователя, его права
     * @param user - пользователь у которого нужно определить правва
     */
    private fun getClaimsFromUser(user: UserPrincipal): List<String> =
        user.authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())

    /**
     * Определяем список прав доступа для токена, его прав
     * @param tokenJWT - по этому токену нужно определить права пользователя
     */
    private fun getClaimsFromToken(tokenJWT: String): List<String> {
        val verifier = getJWTVerifier()
        //С помощью секретного ключа расшифровываем токен
        return verifier.verify(tokenJWT).getClaim(AUTHORITIES).asList(String::class.java)
    }

    /**
     * Проверяем токен JWT.
     * Получаем переданный токен из заголовка
     */
    private fun getJWTVerifier(): JWTVerifier {
        try {
            //secret нужно сохранить в безопасном месте и зашифрованном виде,
            //и доступ к нему иметь из служебного класса
            val algorithm = Algorithm.HMAC512(secret)
            //С помощью секретного ключа расшифровываем токен
            return JWT.require(algorithm).build()
        }catch (e: JWTVerificationException) {
            //Вызываем новое исключения для того чтобы злоумышленник не смог отследить действия в приложении
            throw JWTVerificationException(TOKEN_CANNOT_BE_VERIFIED)
        }
    }
}