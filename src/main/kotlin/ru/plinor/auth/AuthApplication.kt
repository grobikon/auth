package ru.plinor.auth

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import ru.plinor.auth.constant.USER_FOLDER
import ru.plinor.auth.service.ServiceUser
import java.io.File

@SpringBootApplication
class AuthApplication{

/*	@Bean fun run(userService: UserService): CommandLineRunner {
		return CommandLineRunner {
			userService.saveRole(Role(null, "ROLE_USER"))
			userService.saveRole(Role(null, "ROLE_MANAGER"))
			userService.saveRole(Role(null, "ROLE_ADMIN"))
			userService.saveRole(Role(null, "ROLE_SUPER_ADMIN"))

			userService.saveUser(User(
				null,
				"fdgbsdhfghsdgf34123123",
				"Соня",
				"Иванова",
				"grob",
				"1234",
				"grob@mail.ru",
				"",
				null,
				null,
				null,
				roles = arrayListOf(),
				authorities = mutableListOf<String>("read", "update"),
				true,
				true))
			//userService.saveUser(User(null, "Test2", "test2", "1234", arrayListOf()))
			//userService.saveUser(User(null, "Test3", "test3", "1234", arrayListOf()))
			//userService.saveUser(User(null, "Test4", "test4", "1234", arrayListOf()))

			userService.addRoleToUser("Соня", "ROLE_SUPER_ADMIN")
			//userService.addRoleToUser("test", "ROLE_ADMIN")
			//userService.addRoleToUser("test", "ROLE_USER")
			//userService.addRoleToUser("test", "ROLE_MANAGER")
			//userService.addRoleToUser("test2", "ROLE_ADMIN")
			//userService.addRoleToUser("test3", "ROLE_MANAGER")
			//userService.addRoleToUser("test4", "ROLE_USER")
		}
	}*/

	@Bean fun run(userService: ServiceUser): CommandLineRunner {
		return CommandLineRunner {
			try {
			userService.addDefaultUser(
				firstName = "Test",
				lastName = "Test",
				username = "test",
				email = "grobikon@mail.ru",
				role = "ROLE_SUPER_ADMIN",
				isActive = true,
				isNotLocket = true)
			} catch (e: Exception) {
				println(e.message)
			}
		}
	}

	@Bean @Qualifier("passwordEncoder") fun passwordEncoder(): PasswordEncoder {
		return myBCryptPasswordEncoder()
	}

	@Bean @Qualifier("BCrypt") fun myBCryptPasswordEncoder(): BCryptPasswordEncoder {
		return BCryptPasswordEncoder(8)
	}

	/**
	 * Фильтр запросов из разных доменов
	 */
	@Bean fun corsFilter(): CorsFilter? {
		val urlBasedCorsConfigurationSource = UrlBasedCorsConfigurationSource()
		val corsConfiguration = CorsConfiguration()
		corsConfiguration.allowCredentials = true
		//домены которым можно отправлять запрос на сервер
		corsConfiguration.allowedOrigins = listOf(
			"http://localhost:4200",
			"http://localhost:8080",
			"http://localhost:80",
			"http://localhost:5000",
			"http://37.140.199.206:5000",
			"http://37.140.199.206:8080",
			"http://37.140.199.206",
			"http://37.140.199.206:80")
		//заголовки, которые будем отправлять для разрешенных доменов в ответ
		corsConfiguration.allowedHeaders = listOf(
			"Origin", "Access-Control-Allow-Origin", "Content-Type",
			"Accept", "Jwt-Token", "Authorization", "Origin, Accept", "X-Requested-With",
			"Access-Control-Request-Method", "Access-Control-Request-Headers"
		)
		//заголовки, которые будем отправлять для разрешенных доменов в ответ
		corsConfiguration.exposedHeaders = listOf(
			"Origin", "Content-Type", "Accept", "Jwt-Token", "Authorization",
			"Access-Control-Allow-Origin", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"
		)
		//типы запросов которые могут отправлять разрешенные домены
		corsConfiguration.allowedMethods = listOf("GET", "POST", "PUT", "DELETE", "OPTIONS")
		//разрешения действуют для всех маршрутов
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration)
		return CorsFilter(urlBasedCorsConfigurationSource)
	}
}

fun main(args: Array<String>) {
	//создаём папку для хранения фотографий профиля пользователя
	File(USER_FOLDER).mkdirs()
	runApplication<AuthApplication>(*args)
}
