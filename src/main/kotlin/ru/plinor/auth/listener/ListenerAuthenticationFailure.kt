package ru.plinor.auth.listener

import org.springframework.context.event.EventListener
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent
import org.springframework.stereotype.Component
import ru.plinor.auth.service.ServiceLoginAttempt

/**
 * Этот класс будет срабатывать когда пользователь не сможет войти в приложение
 * @param loginAttemptService - Сервис, который будет отслеживать попытки входа в систему для пользователя.
 */
@Component
class ListenerAuthenticationFailure(
    private val loginAttemptService: ServiceLoginAttempt
) {

    /**
     * Сработает, когда пользователь захочет пройти аутентификацию с неправильными данными учётной записи
     * @param event - событие, вызвана аутентификация с неправильными данными учётной записи,
     *                  будет вызываться при каждой аутентификации.
     *
     * @EventListener - означает что мы будем слушать событие AuthenticationFailureBadCredentialsEvent,
     *                  всякий раз когда оно произойдет, будем попадать в этот метод.
     */
    @EventListener
    fun onAuthenticationFailure(event: AuthenticationFailureBadCredentialsEvent) {
        //Так при /login мы используем UsernamePasswordAuthenticationToken  в который передаем username: String
        //event.authentication.principal будет Object который хранит строку с username
        val principal: Any = event.authentication.principal
        if (principal is String) {
            val username: String = principal
            //после того как мы определили имя пользователя, который пытался войти в систему
            //мы передаем в сервис loginAttemptService, что была неудачная попытка входа
            loginAttemptService.addUserToLoginAttemptCache(username)
        }
    }
}