package ru.plinor.auth.listener

import org.springframework.context.event.EventListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.stereotype.Component
import ru.plinor.auth.model.UserPrincipal
import ru.plinor.auth.service.ServiceLoginAttempt

/**
 * Этот класс будет срабатывать когда пользователь успешно прошёл аутентификацию
 * @param loginAttemptService - Сервис, который будет отслеживать попытки входа в систему для пользователя.
 */
@Component
class ListenerAuthenticationSuccess(
    private val loginAttemptService: ServiceLoginAttempt
) {

    /**
     * Сработает, когда пользователь успешно прошёл аутентификацию. Нам нужно будет удалить cache неудачных попыток
     * @param event - событие, пользователь успешно прошёл аутентификацию,
     *                  будет вызываться при каждой аутентификации.
     *
     * @EventListener - означает что мы будем слушать событие AuthenticationSuccessEvent,
     *                  всякий раз когда оно произойдет, будем попадать в этот метод.
     */
    @EventListener
    fun onAuthenticationSuccess(event: AuthenticationSuccessEvent) {
        //Так при /login мы используем UsernamePasswordAuthenticationToken в который передаем username: String
        //event.authentication.principal будет Object который хранит UserPrincipal т.к. была успешная аутентификация
        val principal: Any = event.authentication.principal
        if (principal is UserPrincipal) {
            val user: UserPrincipal = principal
            //после того как мы определили имя пользователя, который пытался войти в систему
            //мы передаем в сервис loginAttemptService, что была удачная аутентификация и нужно удалить cache этого пользователя
            loginAttemptService.evictUserFromLoginAttemptCache(user.username)
        }
    }
}