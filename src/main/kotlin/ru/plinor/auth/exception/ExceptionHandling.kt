package ru.plinor.auth.exception

import com.auth0.jwt.exceptions.TokenExpiredException
import org.slf4j.LoggerFactory
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.web.HttpRequestMethodNotSupportedException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestControllerAdvice
import ru.plinor.auth.model.HttpResponse
import ru.plinor.auth.exception.domain.*
import java.io.IOException
import java.lang.String.format
import java.util.*
import javax.persistence.NoResultException


/**
 * Исключения, которые генерирует Spring.
 * Будем обрабатывать все исключения возникающие в приложении.
 * При помощи @ExceptionHandler() будем отлавливать тип исключения, который создаем Spring,
 * пример DisabledException, BadCredentialsException и т.д.
 *
 * @RestControllerAdvice - будет вести себя как контролер для обработки исключений
 * ErrorController - обрабатываем ошибку 404 + fun notFound404()
 */
@RestControllerAdvice
class ExceptionHandling: ErrorController {
    private val logger = LoggerFactory.getLogger(ExceptionHandling::class.java)

    /**
     * Ваша учетная запись была отключена.
     * @ExceptionHandler(DisabledException::class) - Если пользователь пытается войти в систему и его
     *                                                    учётная запись отключена, то в spring сработает это исключение
     */
    @ExceptionHandler(DisabledException::class)
    fun accountDisabledException(): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.BAD_REQUEST, ACCOUNT_DISABLED)

    /**
     * Неверное имя пользователя/пароль.
     */
    @ExceptionHandler(BadCredentialsException::class)
    fun badCredentialsException(): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.BAD_REQUEST, INCORRECT_CREDENTIALS)

    /**
     * У вас недостаточно разрешения.
     */
    @ExceptionHandler(AccessDeniedException::class)
    fun accessDeniedException(): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.FORBIDDEN, NOT_ENOUGH_PERMISSION)

    /**
     * Ваша учетная запись была заблокирована.
     */
    @ExceptionHandler(LockedException::class)
    fun lockedException(): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.UNAUTHORIZED, ACCOUNT_LOCKED)

    /**
     * Срок действия токена истёк.
     */
    @ExceptionHandler(TokenExpiredException::class)
    fun tokenExpiredException(exception: TokenExpiredException): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.UNAUTHORIZED, exception.message.toString())

    /**
     * Имя почты уже занято.
     */
    @ExceptionHandler(EmailExistException::class)
    fun emailExistException(exception: EmailExistException): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.BAD_REQUEST, exception.message.toString())

    /**
     * Имя пользователя уже занято.
     */
    @ExceptionHandler(UsernameExistException::class)
    fun usernameExistException(exception: UsernameExistException): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.BAD_REQUEST, exception.message.toString())

    /**
     * Email не найден в БД.
     */
    @ExceptionHandler(EmailNotFoundException::class)
    fun emailNotFoundException(exception: EmailNotFoundException): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.BAD_REQUEST, exception.message.toString())

    /**
     * Пользователь не найден в БД.
     */
    @ExceptionHandler(UserNotFoundException::class)
    fun userNotFoundException(exception: UserNotFoundException): ResponseEntity<HttpResponse> =
        createHttpResponse(HttpStatus.BAD_REQUEST, exception.message.toString())

    /**
     * Этот метод запроса не разрешен на этой конечной точке.
     */
/*    @ExceptionHandler(NoHandlerFoundException::class)
    fun methodNotSupportedException(exception: NoHandlerFoundException): ResponseEntity<HttpResponse> {
        return createHttpResponse(HttpStatus.BAD_REQUEST, format(METHOD_IS_NOT_ALLOWED, "Эта страница не найдена."))
    }*/

    /**
     * Этот метод запроса не разрешен на этой конечной точке.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException::class)
    fun methodNotSupportedException(exception: HttpRequestMethodNotSupportedException): ResponseEntity<HttpResponse> {
        //пытаемся отправить пользователю лучшее сообщение
        //iterator().next() получаем первый элемент
        val supportedMethod: HttpMethod = Objects.requireNonNull(exception.supportedHttpMethods).iterator().next()
        return createHttpResponse(HttpStatus.METHOD_NOT_ALLOWED, format(METHOD_IS_NOT_ALLOWED, supportedMethod))
    }

    /**
     * При обработке запроса произошла ошибка.
     */
    @ExceptionHandler(Exception::class)
    fun internalServerErrorException(exception: Exception): ResponseEntity<HttpResponse> {
        logger.error(exception.message)
        return createHttpResponse(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MSG)
    }

    @ExceptionHandler(NotAnImageFileException::class)
    fun notAnImageFileException(exception: NotAnImageFileException): ResponseEntity<HttpResponse> {
        logger.error(exception.message.toString())
        return createHttpResponse(HttpStatus.BAD_REQUEST, exception.message.toString())
    }

    @ExceptionHandler(NoResultException::class)
    fun notFoundException(exception: NoResultException): ResponseEntity<HttpResponse> {
        logger.error(exception.message)
        return createHttpResponse(HttpStatus.NOT_FOUND, exception.message.toString())
    }

    /**
     * Произошла ошибка при обработке файла.
     */
    @ExceptionHandler(IOException::class)
    fun iOException(exception: IOException): ResponseEntity<HttpResponse> {
        logger.error(exception.message)
        return createHttpResponse(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_PROCESSING_FILE)
    }

    @RequestMapping(ERROR_PATH)
    fun notFound404(): ResponseEntity<HttpResponse> {
        return createHttpResponse(HttpStatus.NOT_FOUND, "Для этого URL-адреса нет сопоставления.")
    }

    /**
     * Создаем ответ пользователю с ошибкой.
     * @param httpStatus - статус ошибки
     * @param message - сообщение с ошибкой
     */
    private fun createHttpResponse(httpStatus: HttpStatus, message: String) : ResponseEntity<HttpResponse> {
        val httpResponse = HttpResponse(
            httpStatusCode = httpStatus.value(),
            httpStatus = httpStatus,
            reasonPhrase = httpStatus.reasonPhrase,
            message = message
        )
        return ResponseEntity(httpResponse, httpStatus)
    }
}