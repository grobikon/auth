package ru.plinor.auth.exception.domain

/**
 * Обрабатываем ошибку, когда не можем найти почту пользователя
 */
class EmailNotFoundException(override val message: String?): Exception()