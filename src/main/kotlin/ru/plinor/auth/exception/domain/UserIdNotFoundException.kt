package ru.plinor.auth.exception.domain

/**
 * Обрабатываем ошибку, когда не можем найти userId пользователя
 */
class UserIdNotFoundException(override val message: String?): Exception()