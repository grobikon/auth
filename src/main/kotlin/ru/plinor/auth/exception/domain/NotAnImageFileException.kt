package ru.plinor.auth.exception.domain

class NotAnImageFileException(override val message: String?): Exception()