package ru.plinor.auth.exception.domain

/**
 * Имя почты уже занято
 */
class EmailExistException(override val message: String?): Exception()