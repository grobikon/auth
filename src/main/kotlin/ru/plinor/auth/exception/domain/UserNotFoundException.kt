package ru.plinor.auth.exception.domain

/**
 * Обрабатываем ошибку, когда не можем найти пользователя
 */
class UserNotFoundException(override val message: String?): Exception()