package ru.plinor.auth.exception.domain

/**
 * Имя пользователя уже занято
 */
class UsernameExistException(override val message: String?): Exception()