package ru.plinor.auth.exception

/**
 * Константы для ExceptionHandling. Будем использовать для создания сообщений с ошибками.
 */
const val ACCOUNT_LOCKED = "Ваша учетная запись была заблокирована. Пожалуйста, свяжитесь с администрацией."
const val METHOD_IS_NOT_ALLOWED = "Этот метод запроса не разрешен на этой конечной точке. Пожалуйста, отправьте \"%s\" запрос."
const val INTERNAL_SERVER_ERROR_MSG = "При обработке запроса произошла ошибка."
const val INCORRECT_CREDENTIALS = "Неверное имя пользователя/пароль. Пожалуйста, попробуйте еще раз."
const val ACCOUNT_DISABLED = "Ваша учетная запись была отключена. Если это ошибка, пожалуйста, свяжитесь с администрацией."
const val ERROR_PROCESSING_FILE = "Произошла ошибка при обработке файла."
const val NOT_ENOUGH_PERMISSION = "У вас недостаточно разрешения."
const val ERROR_PATH = "/error"
