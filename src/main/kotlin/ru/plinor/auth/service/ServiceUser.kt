package ru.plinor.auth.service

import org.springframework.web.multipart.MultipartFile
import ru.plinor.auth.model.Role
import ru.plinor.auth.model.User

interface ServiceUser {
    fun registerUser(firstName: String, lastName: String, username: String, email: String): User?
    fun addNewUser(firstName: String,
                   lastName: String,
                   username: String,
                   email: String,
                   role: String,
                   isNotLocket: Boolean,
                   isActive: Boolean,
                   profileImage: MultipartFile?): User?
    fun addDefaultUser(firstName: String,
                   lastName: String,
                   username: String,
                   email: String,
                   role: String,
                   isNotLocket: Boolean,
                   isActive: Boolean): User?
    fun updateUser(currentUsername: String,
                   newFirstName: String,
                   newLastName: String,
                   newUsername: String,
                   newEmail: String,
                   newRole: String,
                   isNotLocket: Boolean,
                   isActive: Boolean,
                   profileImage: MultipartFile?): User?
    fun deleteUser(userId: String)
    fun resetPassword(email: String)                    //новый пароль отправим по email
    fun updateProfileImage(username: String, profileImage: MultipartFile): User      //обновляем только фото пользователя
    fun getUsers(): List<User>?
    fun findUserByUsername(username: String): User?
    fun findUserByEmail(email: String): User?
    fun findUserByUserId(userId: String): User?
    fun saveUser(user: User): User
    fun saveRole(role: Role): Role
    fun addRoleToUser(username: String, roleName: String)
    fun getUser(username: String): User?
}