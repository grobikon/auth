package ru.plinor.auth.service

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

/**
 * Сервис, который будет отслеживать попытки входа в систему.
 * Будем отлавливать Brute Force Attack(атаки грубой силы) перебор паролей пользователя
 * @param loginAttemptCache - кэш для хранения попыток входа в систему, утилита от google.
 *                            expireAfterWrite(15) - срок хранения кэш в системе 15.
 *                            maximumSize(100) - количество записей в кэш 100 в любой момент времени
 *                            build - инициализируем первое значение как 0
 *
 * !!!Можно доработать и использовать блокировку по ip адресу!!!, из запроса получаем ip - адресс
 */
@Service
class ServiceLoginAttempt(
    private val loginAttemptCache: LoadingCache<String, Int> =
        CacheBuilder.newBuilder()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .maximumSize(100)
            .build(object : CacheLoader<String, Int>(){
                override fun load(key: String): Int = 0
            })
) {
    companion object{
        const val MAXIMUM_NUMBER_OF_ATTEMPTS = 5        //максимальное кол-во ввода пароля для пользователя, иначе блокируем учётную запись
        const val ATTEMPTS_INCREMENT = 1                //нужен когда неудачно пытались войти в систему
    }

    /**
     * Добавляем пользователя в cache, после неудачной попытки входа
     * @param username - будем искать значение в кэш по логину пользователя
     */
    fun addUserToLoginAttemptCache(username: String) {
        //увеличиваем инкремент попытки входа
        val attempt = ATTEMPTS_INCREMENT + loginAttemptCache.get(username)
        //сохраняем инкремент в cache
        loginAttemptCache.put(username, attempt)
    }

    /**
     * Удаляем пользователя из "Cache".
     * @param username - будем искать значение в кэш по логину пользователя
     */
    fun evictUserFromLoginAttemptCache(username: String){
        //удаляем значение кэш по логину
        loginAttemptCache.invalidate(username)
    }

    /**
     * Определяем было произведено максимальное кол-во попыток входа или нет
     * @param username - будем искать значение в кэш по логину пользователя
     */
    fun hasExceededMaxAttempt(username: String): Boolean = loginAttemptCache.get(username) >= MAXIMUM_NUMBER_OF_ATTEMPTS

}
