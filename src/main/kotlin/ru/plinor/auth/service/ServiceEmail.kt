package ru.plinor.auth.service

import com.sun.mail.smtp.SMTPTransport
import org.springframework.stereotype.Service
import ru.plinor.auth.constant.*
import java.util.*
import javax.mail.Message
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

/**
 * Сервис, электронной почты.
 */
@Service
class ServiceEmail {

    /**
     * Отправляем новый пароль пользователю по почте
     * @param firstname - имя пользователя кому будем отправлять письмо
     * @param password - пароль пользователя который был сгенерирован системой при регистрации нового пользователя администратором,
     *                   будет отправлен в теле письма.
     * @param email - почта пользователя куда будем отправлять письмо.
     */
    fun sendNewPasswordEmail(firstname: String, password: String, email: String) {
        val message: Message = createEmail(firstname = firstname, password = password, email = email)                       //создаём сообщение для отправки
        val smtpTransport: SMTPTransport = getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL) as SMTPTransport   //сюда будем отправлять сообщение
        smtpTransport.connect(GMAIL_SMTP_SERVER, USERNAME, PASSWORD)                                                        //вводим сервер, имя пользователя и пароль
        smtpTransport.sendMessage(message, message.allRecipients)                                                           //отправляем сообщение пользователю
        smtpTransport.close()
    }

    /**
     * Создаём электронную почту
     * @param firstname - имя пользователя кому будем отправлять письмо
     * @param password - пароль пользователя который был сгенерирован системой при регистрации нового пользователя администратором,
     *                   будет отправлен в теле письма.
     * @param email - почта пользователя куда будем отправлять письмо.
     */
    private fun createEmail(firstname: String, password: String, email: String): Message {
        val message: Message = MimeMessage(getEmailSession())
        message.setFrom(InternetAddress(FROM_EMAIL))
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false))            //устанавливаем кому мы отправляем письмо
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(CC_EMAIL, false))         //устанавливаем кому мы отправляем письмо
        message.subject = EMAIL_SUBJECT                                                                       //тема сообщения
        message.setText("Здравствуйте $firstname, \n\n Ваш новый пароль: $password\n\n Команда поддержки.")   //текст сообщения
        message.sentDate = Date()                                                                             //дата отправки
        message.saveChanges()
        return message
    }

    /**
     * Сеанс получения настроек почты отправителя
     */
    private fun getEmailSession(): Session {
        //свойства, будем устанавливать свойства из константы ConstantEmail
        val properties: Properties = System.getProperties()
        properties[SMTP_HOST] = GMAIL_SMTP_SERVER
        properties[SMTP_AUTH] = true                //используем аутентификацию
        properties[SMTP_PORT] = DEFAULT_PORT        //порт почты
        properties[SMTP_STARTTLS_ENABLE] = true     //безопасность транспортного уровня, мы не хотим оставлять открытую связь
        properties[SMTP_STARTTLS_REQUIRED] = true   //проверяем что безопасная передача включена
        return Session.getInstance(properties)
    }
}