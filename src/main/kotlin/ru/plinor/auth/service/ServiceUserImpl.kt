package ru.plinor.auth.service

import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.StringUtils
import org.apache.tomcat.util.http.fileupload.FileUtils
import org.aspectj.util.FileUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType.*
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import ru.plinor.auth.constant.*
import ru.plinor.auth.exception.domain.*
import ru.plinor.auth.model.Role
import ru.plinor.auth.model.User
import ru.plinor.auth.model.UserPrincipal
import ru.plinor.auth.myEnum.EnumRole
import ru.plinor.auth.repo.RoleRepo
import ru.plinor.auth.repo.UserRepo
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.*

/**
 * UserDetailsService - реализуем этот интерфейс, чтобы найти пользователя при аутентификации его в spring
 * @param userRepo - для работы пользователя с БД
 * @param loginAttemptService - Сервис, который будет отслеживать попытки входа в систему для пользователя.
 * @param emailService - Сервис, который будет отправлять письма пользователю.
 */
@Service
@Transactional
@Qualifier("userDetailsService")
class ServiceUserImpl(
    private val userRepo: UserRepo,
    private val roleRepo: RoleRepo,
    @Qualifier("passwordEncoder") private val passwordEncoder: PasswordEncoder,  //кодировщик паролей
    private val loginAttemptService: ServiceLoginAttempt,
    private val emailService: ServiceEmail
): ServiceUser, UserDetailsService {
    private val logger = LoggerFactory.getLogger(ServiceUserImpl::class.java)

    override fun registerUser(firstName: String, lastName: String, username: String, email: String): User? {
        //Проверяем что в БД нет пользователя с такими данными username и email, если есть выбросим ошибку
        validateNewUsernameAndEmail(newUsername = username, newEmail = email)
        val newPassword = generatePassword()
        val newUser = User(
            userId = generateUserId(),
            firstName = firstName,
            lastName =  lastName,
            username = username,
            email = email,
            joinDate = Date(),
            password = encoderPassword(newPassword),
            isActive = true,
            isNotLocked = true,
            role = EnumRole.ROLE_USER.name,
            authorities = EnumRole.ROLE_USER.authorities.toMutableList(),
            profileImageUrl = getTemporaryProfileImageUrl(username)
        )
        //Сохраняем нового пользователя в БД
        userRepo.save(newUser)
        //отправляем письмо пользователю с паролем
        emailService.sendNewPasswordEmail(firstname = firstName, password = newPassword, email = email)
        logger.info("Новый пароль для пользователя: $newPassword")
        return newUser
    }

    override fun addNewUser(
        firstName: String,
        lastName: String,
        username: String,
        email: String,
        role: String,
        isNotLocket: Boolean,
        isActive: Boolean,
        profileImage: MultipartFile?
    ): User? {
        //Проверяем что в БД нет пользователя с такими данными username и email, если есть выбросим ошибку
        validateNewUsernameAndEmail(newUsername = username, newEmail = email)
        val newPassword = generatePassword()
        val newUser = User(
            userId = generateUserId(),
            firstName = firstName,
            lastName =  lastName,
            username = username,
            email = email,
            joinDate = Date(),
            password = encoderPassword(newPassword),
            isActive = isActive,
            isNotLocked = isNotLocket,
            role = EnumRole.getRoleEnumName(role).name,
            authorities = EnumRole.getRoleEnumName(role).authorities.toMutableList(),
            profileImageUrl = getTemporaryProfileImageUrl(username)
        )
        //Сохраняем нового пользователя в БД
        userRepo.save(newUser)
        //сохраняем изображение пользователя после того как сохраним самого пользователя в БД
        saveProfileImage(newUser, profileImage)
        //отправляем письмо пользователю с паролем
        emailService.sendNewPasswordEmail(firstname = firstName, password = newPassword, email = email)
        logger.info("Новый пароль для пользователя: $newPassword")
        return newUser
    }

    override fun addDefaultUser(
        firstName: String,
        lastName: String,
        username: String,
        email: String,
        role: String,
        isNotLocket: Boolean,
        isActive: Boolean
    ): User? {
        //Проверяем что в БД нет пользователя с такими данными username и email, если есть выбросим ошибку
        validateNewUsernameAndEmail(newUsername = username, newEmail = email)
        val newPassword = generatePassword()
        val newUser = User(
            userId = generateUserId(),
            firstName = firstName,
            lastName =  lastName,
            username = username,
            email = email,
            joinDate = Date(),
            password = encoderPassword(newPassword),
            isActive = isActive,
            isNotLocked = isNotLocket,
            role = EnumRole.getRoleEnumName(role).name,
            authorities = EnumRole.getRoleEnumName(role).authorities.toMutableList()
        )
        //Сохраняем нового пользователя в БД
        userRepo.save(newUser)
        //отправляем письмо пользователю с паролем
        emailService.sendNewPasswordEmail(firstname = firstName, password = newPassword, email = email)
        logger.info("Новый пароль для пользователя: $newPassword")
        return newUser
    }

    override fun updateUser(
        currentUsername: String,
        newFirstName: String,
        newLastName: String,
        newUsername: String,
        newEmail: String,
        newRole: String,
        isNotLocket: Boolean,
        isActive: Boolean,
        profileImage: MultipartFile?
    ): User? {
        //Проверяем что в БД нет пользователя с такими данными username и email, если есть выбросим ошибку
        val currentUser = validateNewUsernameAndEmail(currentUsername = currentUsername, newUsername = newUsername, newEmail = newEmail)
        currentUser!!.userId = generateUserId()
        currentUser.firstName = newFirstName
        currentUser.lastName =  newLastName
        currentUser.username = newUsername
        currentUser.email = newEmail
        currentUser.joinDate = Date()
        currentUser.isActive = isActive
        currentUser.isNotLocked = isNotLocket
        currentUser.role = EnumRole.getRoleEnumName(newRole).name
        currentUser.authorities = EnumRole.getRoleEnumName(newRole).authorities.toMutableList()
        //Сохраняем нового пользователя в БД
        userRepo.save(currentUser)
        //сохраняем изображение пользователя после того как сохраним самого пользователя в БД
        saveProfileImage(currentUser, profileImage)
        return currentUser
    }

    override fun deleteUser(userId: String) {
        //поиск пользователя для удаления
        val user = findUserByUserId(userId)?:throw UserIdNotFoundException(NO_USER_FOUND_BY_USER_ID + userId)
        val userFolder: Path = Paths.get(USER_FOLDER + user.username).toAbsolutePath().normalize()
        FileUtils.deleteDirectory(File(userFolder.toString()))
        userRepo.deleteById(user.id!!)
    }

    override fun resetPassword(email: String) {
        val user: User = userRepo.findByEmail(email)?: throw EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email)
        val newPassword = generatePassword()
        user.password = encoderPassword(newPassword)
        //Сохраняем пользователя с новым паролем в БД
        userRepo.save(user)
        //отправляем письмо пользователю с паролем
        emailService.sendNewPasswordEmail(firstname = user.firstName, password = newPassword, email = user.email)
    }

    override fun updateProfileImage(username: String, profileImage: MultipartFile): User {
        val user = validateNewUsernameAndEmail(currentUsername = username)
        saveProfileImage(user!!, profileImage)
        return user
    }

    /**
     * Сохраняем изображение пользователя
     * @param user - пользователь у которого будем сохранять изображение
     * @param profileImage - изображение, которое будем сохранять
     */
    private fun saveProfileImage(user: User, profileImage: MultipartFile?) {
        profileImage?:return
        //проверяем profileImage является картинкой или нет
        if (!listOf(IMAGE_JPEG_VALUE, IMAGE_PNG_VALUE, IMAGE_GIF_VALUE).contains(profileImage.contentType)){
            //Если переданный файл не картинка показываем исключение
            throw NotAnImageFileException(profileImage.originalFilename + "не является файлом изображения. Пожалуйста загрузите изображение.")
        }
        //С начало ищем папку пользователя с изображением
        //example: userFolder = /user/home/plinorimageuser/user/username/
        val userFolder: Path = Paths.get(USER_FOLDER + user.username).toAbsolutePath().normalize()
        //проверяем существует этот путь в файловой системе
        if (!Files.exists(userFolder)) {
            //Создаем этот путь в файловой системе
            Files.createDirectories(userFolder)
            logger.info(DIRECTORY_CREATED  + userFolder)
        }
        //С начало удаляем ранее созданный файл
        Files.deleteIfExists(Paths.get(userFolder.toString() + user.username + DOT + JPG_EXTENSION))
        //Сохраняем новую картинку, если она там есть она перезапишеться StandardCopyOption.REPLACE_EXISTING
        Files.copy(profileImage.inputStream, userFolder.resolve(user.username + DOT + JPG_EXTENSION), StandardCopyOption.REPLACE_EXISTING)
        //обновляем путь url до картинки пользователя в БД
        user.profileImageUrl = setProfileImageUrl(user.username)
        userRepo.save(user)
        logger.info(FILE_SAVED_IN_FILE_SYSTEM + profileImage.originalFilename)
    }

    private fun setProfileImageUrl(username: String): String =
        ServletUriComponentsBuilder.fromCurrentContextPath()
            .path(USER_IMAGE_PATH + username + FORWARD_SLASH + username + DOT + JPG_EXTENSION).toUriString()   //Http:localhost:8081/

    private fun generateUserId(): String = UUID.randomUUID().toString().replace("-", "")

    private fun generatePassword(): String = RandomStringUtils.randomAlphanumeric(10)

    private fun encoderPassword(password: String): String = passwordEncoder.encode(password)

    private fun getTemporaryProfileImageUrl(username: String): String =
        ServletUriComponentsBuilder.fromCurrentContextPath().path(DEFAULT_PATH_USER_IMAGE + username).toUriString()   //Http:localhost:8081/

    /**
     * Проверяем пользователя на уникальность в БД, если пользователь уникален возвращаем его иначе показываем ошибку
     * @param currentUsername - имя текущего пользователя
     * @param newUsername - имя нового пользователя
     * @param newEmail - email нового пользователя
     */
    private fun validateNewUsernameAndEmail(currentUsername: String? = null, newUsername: String? = null, newEmail: String? = null): User? {
        val userByNewUsername: User? = if (newUsername != null) findUserByUsername(newUsername) else null
        val userByNewEmail: User? = if (newEmail != null) findUserByEmail(newEmail) else null
        //проверяем имя текущего пользователя передали его или нет
        when(StringUtils.isNotBlank(currentUsername)) {
            true -> {
                //Обновляем текущего пользователя
                //Для этого нужно произвести проверки есть этот пользователь в БД, новое имя и email должны быть не заняты
                val currentUser: User = findUserByUsername(currentUsername!!)
                    ?: throw UserNotFoundException(NO_USER_FOUND_BY_USERNAME + currentUsername)
                if (userByNewUsername != null && currentUser.id!! != userByNewUsername.id!!) {
                    throw UsernameExistException(ALREADY_EXISTS_USERNAME + newUsername)
                }
                if (userByNewEmail != null && currentUser.id!! != userByNewEmail.id!!) {
                    throw EmailExistException(ALREADY_EXISTS_EMAIL + newEmail)
                }
                return currentUser
            }
            false -> {
                //Создаём нового пользователя
                //Для этого нужно произвести проверки в БД, новое имя и email должны быть не заняты
                if (userByNewUsername != null) throw UsernameExistException(ALREADY_EXISTS_USERNAME + newUsername)
                if (userByNewEmail != null) throw EmailExistException(ALREADY_EXISTS_EMAIL + newEmail)
                return null
            }
        }
    }

    override fun getUsers(): List<User>? {
        logger.info("Загрузили всех пользователей из БД")
        return userRepo.getUsers()
    }

    override fun findUserByUsername(username: String): User? {
        logger.info("Загрузили пользователя $username из БД")
        return userRepo.findByUsername(username)
    }

    override fun findUserByEmail(email: String): User? {
        logger.info("Загрузили пользователя по email $email из БД")
        return userRepo.findByEmail(email)
    }

    override fun findUserByUserId(userId: String): User? {
        logger.info("Загрузили пользователя по userId $userId из БД")
        return userRepo.findByUserId(userId)
    }

    /**
     * Этот метод вызывается всякий раз, когда пользователь хочет зайти. Spring security пытается проверить аутентификацию.
     * Чтобы найти фактического пользователя
     * @param username - пользователь которого нужно найти в БД, если его нет будет показывать ошибку UsernameNotFoundException
     */
    override fun loadUserByUsername(username: String?): UserDetails {
        val user = userRepo.findByUsername(username)
        return when(user == null) {
            true -> {
                logger.error(NO_USER_FOUND_BY_USERNAME + username)
                throw UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME + username)
            }
            false -> {
                //Проверяем что пользователь не заблокирован. Проверяем количество попыток входа.
                validateLoginAttempt(user)
                logger.info("Пользователь $username найден в БД")
                //Нашли пользователя по имени, теперь нужно вернуть информацию о пользователе
                //Для отображения в интерфейсе пользователя последней даты входа
                user.lastDateLoginDisplay = user.lastDateLogin
                user.lastDateLogin = Date()
                userRepo.save(user)
                UserPrincipal(user = user)

                //Устанавливаем власть для пользователя с его ролями
                //Для каждой роли пользователя создадим полномочия, список правил
                //val authorities: MutableCollection<SimpleGrantedAuthority> = mutableListOf()
                //user.roles?.forEach { role -> authorities.add(SimpleGrantedAuthority(role.name)) }
                //org.springframework.security.core.userdetails.User(user.username, user.password, authorities)
            }
        }
    }

    /**
     * Проверяем что пользователь не заблокирован
     * @param user - пользователь, которого будем проверять. Если он уже заблокирован удаляем кэш с попытками входа.
     *              Если он не заблокирован проверяем в кэш попыток входа кол-во попыток если оно максимальное блокируем учётку.
     */
    private fun validateLoginAttempt(user: User) {
        when (user.isNotLocked) {
            true -> {
                //Проверяем не превысили они 5 неудачных попыток входа
                when(loginAttemptService.hasExceededMaxAttempt(user.username)){
                    true -> user.isNotLocked = false //блокируем учётную запись
                    false -> user.isNotLocked = true //пользователь не заблокирован
                }
            }
            //Удаляем кэш попыток входа для этого пользователя
            false -> loginAttemptService.evictUserFromLoginAttemptCache(user.username)
        }
    }

    override fun saveUser(user: User): User {
        logger.info("Сохраняем нового пользователя ${user.username} в БД")
        user.password = passwordEncoder.encode(user.password)   //Кодируем пароль пользователя. Чтобы в БД мы не видели его.
        return userRepo.save(user)
    }

    override fun saveRole(role: Role): Role {
        logger.info("Сохраняем новую роль ${role.name} в БД")
        return roleRepo.save(role)
    }

    override fun addRoleToUser(username: String, roleName: String) {
        logger.info("Добавили роль $roleName для пользователя $username в БД")
        //val user = userRepo.findByUsername(username)?:return
        //val role = roleRepo.findByName(roleName)?:return
        /*when(user.roles == null) {
            true -> {
                user.roles = mutableListOf()
                user.roles!!.add(role)
            }
            false -> user.roles!!.add(role)
        }*/
    }

    override fun getUser(username: String): User? {
        logger.info("Загрузили пользователя $username из БД")
        return userRepo.findByUsername(username)
    }
}