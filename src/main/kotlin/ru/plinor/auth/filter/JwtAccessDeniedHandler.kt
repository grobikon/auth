package ru.plinor.auth.filter

import com.fasterxml.jackson.databind.ObjectMapper
import lombok.extern.slf4j.Slf4j
import org.hibernate.bytecode.BytecodeLogger.LOGGER
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.stereotype.Component
import ru.plinor.auth.constant.ACCESS_DENIED_MESSAGE
import ru.plinor.auth.model.HttpResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Попадаем в этот класс всякий раз когда пользователю запрещён доступ
 */
@Slf4j
@Component
class JwtAccessDeniedHandler: AccessDeniedHandler{

    /**
     * Будем сами обрабатывать и составлять сообщение пользователю в таком случае.
     * Будем использовать HttpResponse - общий ответ который мы будем отправлять пользователю при любом запросе к серверу.
     * Будем отправлять ответ в виде json файла.
     */
    override fun handle(
        request: HttpServletRequest?,
        response: HttpServletResponse?,
        exception: AccessDeniedException?
    ) {
        LOGGER.debug("JwtAuthorizationEntryPoint - Http403ForbiddenEntryPoint")
        val httpResponse = HttpResponse(
            httpStatusCode = HttpStatus.UNAUTHORIZED.value(),
            httpStatus = HttpStatus.UNAUTHORIZED,
            reasonPhrase = HttpStatus.UNAUTHORIZED.reasonPhrase.uppercase(),
            message = ACCESS_DENIED_MESSAGE
        )
        response?.contentType = APPLICATION_JSON_VALUE
        response?.status = HttpStatus.UNAUTHORIZED.value()
        val outputStream = response?.outputStream
        val mapper = ObjectMapper()
        mapper.writeValue(outputStream, httpResponse)
        outputStream?.flush()
    }
}