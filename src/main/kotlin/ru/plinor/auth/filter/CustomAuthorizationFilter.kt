package ru.plinor.auth.filter

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import com.fasterxml.jackson.databind.ObjectMapper
import lombok.extern.slf4j.Slf4j
import org.hibernate.bytecode.BytecodeLogger.LOGGER
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import java.util.Arrays.stream
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Фильтр авторизации по имени пользователя и поролю,
 * Будет перехватывать каждый запрос поступающий в приложение
 */
@Slf4j
class CustomAuthorizationFilter: OncePerRequestFilter() {

    /**
     * Определяем имеет ли пользователь доступ к приложению или нет
     */
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        //Проверяем запрос для входа в систему или нет
        when(request.servletPath.equals("/api/login")
                || request.servletPath.equals("/api/token/refresh") ) {
            //тогда не надо ни чего делать переходим далее в цепочку фильтров на слудующий фильтр,
            //пользователь пытаеться войти в систему
            true -> filterChain.doFilter(request, response)
            false -> {
                //Проверяем наличие токена в запросе в заголовке
                val authorizationHeader = request.getHeader(AUTHORIZATION)
                when(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
                    true -> {
                        try {
                            //Получаем переданный токен из заголовка
                            val tokenJwt = authorizationHeader.substring("Bearer ".length)
                            val algorithm = Algorithm.HMAC256("secret".toByteArray())       //secret нужно сохранить в безопасном месте и зашифрованном виде, и доступ к нему иметь из служебного класса
                            //С помощью секретного ключа расшифровываем токен
                            val verifier: JWTVerifier = JWT.require(algorithm).build()
                            val decoderJWT: DecodedJWT = verifier.verify(tokenJwt)
                            //Получаем имя пользователя
                            val username = decoderJWT.subject
                            //Получаем роли пользователя
                            val roles :Array<String?> = decoderJWT.getClaim("roles").asArray(String::class.java)
                            //Проверяем все роли пользователя для доступа,
                            //и даем полномочия через spring в зависимости от роли пользователя
                            //spring будет знать что может делать пользователь
                            val authorities: MutableCollection<SimpleGrantedAuthority> = mutableListOf()
                            stream(roles).forEach { role ->
                                authorities.add(SimpleGrantedAuthority(role))
                            }
                            val authenticationToken = UsernamePasswordAuthenticationToken(username, null, authorities)
                            SecurityContextHolder.getContext().authentication = authenticationToken
                            //после того как мы сообщили spring информацию о пользователе и его правах доступа, вызываем цепочку фильтров
                            filterChain.doFilter(request, response)
                        }catch (e: Exception) {
                            LOGGER.error("Ошибка входа ${e.message}")
                            response.setHeader("error", e.message)
                            response.status = HttpStatus.FORBIDDEN.value()
                            //response.sendError(HttpStatus.FORBIDDEN.value())   //403 запрещенный код, нет доступа
                            val error: MutableMap<String, String> = HashMap()
                            error["error_message"] = e.message.toString()
                            response.contentType = MediaType.APPLICATION_JSON_VALUE                                  //Возвращаем токены в теле ответа в виде json
                            ObjectMapper().writeValue(response.outputStream, error)
                        }
                    }
                    false -> filterChain.doFilter(request, response)
                }
            }
        }
    }
}