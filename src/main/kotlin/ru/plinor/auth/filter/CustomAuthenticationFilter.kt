package ru.plinor.auth.filter

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import lombok.extern.slf4j.Slf4j
import org.hibernate.bytecode.BytecodeLogger
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.util.*
import java.util.stream.Collectors
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Фильтр аутентификации по имени пользователя и поролю
 * @param authManager - AuthenticationManager является просто контейнером для поставщиков аутентификации
 */
@Slf4j
class CustomAuthenticationFilter(
    private val authManager: AuthenticationManager
): UsernamePasswordAuthenticationFilter() {

    /**
     * Попытка аутентификации, когда пользователь пытаеться получить доступ к системе
     */
    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {
        val username = request?.getParameter("username")
        val password = request?.getParameter("password")
        BytecodeLogger.LOGGER.info("Пользователь $username")
        BytecodeLogger.LOGGER.info("Пароль $password")
        //маркер аутентификации имени пользователя и пароля
        val authenticationToken = UsernamePasswordAuthenticationToken(username, password)
        //передаём в authenticate UsernamePasswordAuthenticationToken в AuthenticationProvider по умолчанию,
        //который будет использовать userDetailsService,
        //чтобы получить пользователя на основе имени пользователя и сравнить пароль этого пользователя с паролем в маркере аутентификации.
        return authManager.authenticate(authenticationToken)
    }

    /**
     * Успешная аутентификация,
     * В этом методе мы должны сгенерировать токен, потом отправить его пользователю
     */
    override fun successfulAuthentication(
        request: HttpServletRequest?,
        response: HttpServletResponse?,
        chain: FilterChain?,
        authentication: Authentication?
    ) {
        val user: User = authentication?.principal as User
        val algorithm = Algorithm.HMAC256("secret".toByteArray())       //secret нужно сохранить в безопасном месте и зашифрованном виде, и доступ к нему иметь из служебного класса
        //Создаём токен
        val roles = user.authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())
        val accessToken = JWT.create()
            .withSubject(user.username)                                                 //идентификатор пользователя, уникальное значение пользователя по нему будем искать пользователя
            .withExpiresAt(Date(System.currentTimeMillis() + 5 * 60 * 1000))      //время жизни токена 10 минут когда он будет недействителен
            .withIssuer(request?.requestURL.toString())                                 //автор этого токена Url адрес приложения клиента, которое запрашивает токен
            .withClaim("roles", roles)                                            //Список правил ролей пользователя
            .sign(algorithm)
        //Создаём токен обновления
        val refreshToken = JWT.create()
            .withSubject(user.username)                                                 //идентификатор пользователя, уникальное значение пользователя по нему будем искать пользователя
            .withExpiresAt(Date(System.currentTimeMillis() + 30 * 60 * 1000))     //время жизни токена 30 минут когда он будет недействителен
            .withIssuer(request?.requestURL.toString())                                 //автор этого токена Url адрес приложения клиента, которое запрашивает токен
            .sign(algorithm)                                                            //подписываем этот токен алгоритмом
        //Отправляем токены как заголовки, после удачного входа пользователь получает в заголовках токен и токен обновления
        //response?.setHeader("access_token", accessToken)
        //response?.setHeader("refresh_token", refreshToken)
        val tokens: MutableMap<String, String> = HashMap()
        tokens["access_token"] = accessToken
        tokens["refresh_token"] = refreshToken
        response?.contentType = APPLICATION_JSON_VALUE                                  //Возвращаем токены в теле ответа в виде json
        ObjectMapper().writeValue(response?.outputStream, tokens)
    }
}