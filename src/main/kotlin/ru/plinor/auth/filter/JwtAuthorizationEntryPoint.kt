package ru.plinor.auth.filter

import com.fasterxml.jackson.databind.ObjectMapper
import lombok.extern.slf4j.Slf4j
import org.hibernate.bytecode.BytecodeLogger.LOGGER
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint
import org.springframework.stereotype.Component
import ru.plinor.auth.constant.FORBIDDEN_MESSAGE
import ru.plinor.auth.model.HttpResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Попадаем в этот класс всякий раз когда пользователь
 * не может предоставить аутентификацию и пробует получить доступ к приложению
 */
@Slf4j
@Component
class JwtAuthorizationEntryPoint: Http403ForbiddenEntryPoint() {

    /**
     * Будем сами обрабатывать и составлять сообщение пользователю в таком случае.
     * Будем использовать HttpResponse - общий ответ который мы будем отправлять пользователю при любом запросе к серверу.
     * Будем отправлять ответ в виде json файла.
     */
    override fun commence(
        request: HttpServletRequest?,
        response: HttpServletResponse?,
        exception: AuthenticationException?
    ) {
        LOGGER.debug("JwtAuthorizationEntryPoint - Http403ForbiddenEntryPoint")
        val httpResponse = HttpResponse(
            httpStatusCode = HttpStatus.FORBIDDEN.value(),
            httpStatus = HttpStatus.FORBIDDEN,
            reasonPhrase = HttpStatus.FORBIDDEN.reasonPhrase.uppercase(),
            message = FORBIDDEN_MESSAGE
        )
        response?.contentType = APPLICATION_JSON_VALUE
        response?.status = HttpStatus.FORBIDDEN.value()
        val outputStream = response?.outputStream
        val mapper = ObjectMapper()
        mapper.writeValue(outputStream, httpResponse)
        outputStream?.flush()
    }
}