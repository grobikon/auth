package ru.plinor.auth.filter

import lombok.extern.slf4j.Slf4j
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import ru.plinor.auth.constant.OPTION_HTTP_METHOD
import ru.plinor.auth.constant.TOKEN_PREFIX
import ru.plinor.auth.util.JWTTokenProvider
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Фильтр авторизации по token JWT,
 * Будет перехватывать каждый запрос поступающий в приложение, и обработан только один раз
 * И проверять разрешать или нет работать пользователю дальше с приложением
 * OncePerRequestFilter - если что-то произойдет в этом классе это произойдёт один раз
 */
@Slf4j
@Component
class JwtAuthorizationFilter(
    private val jwtTokenProvider: JWTTokenProvider
): OncePerRequestFilter() {

    /**
     * Определяем имеет ли пользователь доступ к приложению или нет.
     * Будет срабатывать каждый раз когда поступает запрос, только один раз.
     */
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        when(request.method.equals(OPTION_HTTP_METHOD, ignoreCase = true)) {
            //OPTION_HTTP_METHOD - перед каждым запросом отправляется для сбора информации о сервере
            true -> response.status = HttpStatus.OK.value()
            false -> {
                //Проверяем наличие токена в запросе в заголовке
                val authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
                when(authorizationHeader != null && authorizationHeader.startsWith(TOKEN_PREFIX)){
                    true -> {
                        //Получаем переданный токен из заголовка.
                        val tokenJwt = authorizationHeader.substring(TOKEN_PREFIX.length)
                        //Получаем имя пользователя из этого токена с помощью написанного нами jwtTokenProvider
                        val username = jwtTokenProvider.getSubject(tokenJwt)
                        //проверяем JWT token действительный он или нет с помощью написанного нами jwtTokenProvider
                        //Вроде не нужно добавлять в условие && SecurityContextHolder.getContext().authentication == null
                        when(jwtTokenProvider.isTokenJWYValid(username, tokenJwt)) {
                            true -> {
                                //Получаем все права пользователя в системе с помощью написанного нами jwtTokenProvider
                                //и передаем права в spring чтобы можно было их использовать
                                val authorities = jwtTokenProvider.getAuthorities(tokenJwt)
                                //теперь у нас есть все данные, чтобы произвести аутентификацию пользователя
                                //вход в систему с помощью написанного нами jwtTokenProvider
                                val authentication = jwtTokenProvider.getAuthentication(username, authorities, request)
                                //теперь говорим spring что этот пользователь аутентифицирован в системе безопасности
                                SecurityContextHolder.getContext().authentication = authentication
                                //после того как мы сообщили spring информацию о пользователе и его правах доступа,
                                //вызываем цепочку фильтров
                                filterChain.doFilter(request, response) //переходим в цепочку фильтров
                            }
                            false -> {
                                //Очищаем данные в контексте безопасности
                                SecurityContextHolder.clearContext()
                                filterChain.doFilter(request, response) //переходим в цепочку фильтров
                            }
                        }
                    }
                    false -> filterChain.doFilter(request, response) //переходим в цепочку фильтров
                }
            }
        }
    }
}