package ru.plinor.auth.service

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import ru.plinor.auth.model.User
import ru.plinor.auth.myEnum.EnumRole
import ru.plinor.auth.repo.UserRepo
import java.util.*

/*
@ExtendWith(MockitoExtension::class)
internal class ServiceUserImplTest {

    @Mock private lateinit var userRepo: UserRepo
    private lateinit var userService: ServiceUserImpl

    @BeforeEach
    fun setUp(){
    }

    @Test
    fun getUsers() {
        //given
        userService.saveUser(createUser("Test1", EnumRole.ROLE_USER))
        userService.saveUser(createUser("Test2", EnumRole.ROLE_MANAGER))

        //when
        val users = userService.getUsers()

        //then
        Assertions.assertThat(users?.size).isEqualTo(2)
    }



    private fun createUser(nameUser: String, role: EnumRole): User {
        return User(
            null,
            userId = UUID.randomUUID().toString().replace("-", ""),
            firstName = nameUser,
            lastName = "Иванова",
            username = nameUser,
            password = "1234",
            email = "${nameUser}@mail.ru",
            "",
            null,
            null,
            joinDate = Date(),
            isActive = true,
            isNotLocked = true,
            role = role.name,
            authorities = role.authorities.toMutableList())
    }
}*/
