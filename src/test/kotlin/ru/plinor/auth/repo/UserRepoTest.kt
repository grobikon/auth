package ru.plinor.auth.repo

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import ru.plinor.auth.model.User
import ru.plinor.auth.myEnum.EnumRole
import java.util.*

@DataJpaTest
class UserRepoTest{

    @Autowired lateinit var userRepo: UserRepo

    /**
     * После каждого теста очищаем данные в userRepo
     */
    @AfterEach
    fun tearDown(){
        userRepo.deleteAll()
    }

    /**
     * Поиск всех пользователей
     */
    @Test
    fun itShouldCheckGetAllUsers() {
        //given
        userRepo.save(createUser("Test1", EnumRole.ROLE_USER))
        userRepo.save(createUser("Test2", EnumRole.ROLE_MANAGER))

        //when
        val users = userRepo.getUsers()

        //then
        Assertions.assertThat(users.size).isEqualTo(2)
    }

    /**
     * Если пользователей нет
     */
    @Test
    fun itShouldCheckGetNotUsers() {
        //given

        //when
        val users = userRepo.getUsers()

        //then
        Assertions.assertThat(users.size).isEqualTo(0)
    }

    @Test
    fun itShouldCheckSearchUserByUsernameFound() {
        //given
        userRepo.save(createUser("Test1", EnumRole.ROLE_USER))

        //when
        val foundUser = userRepo.findByUsername("Test1")

        //then
        Assertions.assertThat(foundUser?.username).isEqualTo("Test1")
    }

    @Test
    fun itShouldCheckSearchUserByUsernameNotFound() {
        //given

        //when
        val foundUser = userRepo.findByUsername("Test1")

        //then
        Assertions.assertThat(foundUser).isNull()
    }

    @Test
    fun itShouldCheckSearchUserByEmailFound() {
        //given
        userRepo.save(createUser("Test1", EnumRole.ROLE_USER))

        //when
        val foundUser = userRepo.findByEmail("Test1@mail.ru")

        //then
        Assertions.assertThat(foundUser?.email).isEqualTo("Test1@mail.ru")
    }

    @Test
    fun itShouldCheckSearchUserByEmailNotFound() {
        //given

        //when
        val users = userRepo.findByEmail("Test1@mail.ru")

        //then
        Assertions.assertThat(users).isNull()
    }

    @Test
    fun itShouldCheckSearchUserByUserIdFound() {
        //given
        val user = createUser("Test1", EnumRole.ROLE_USER)
        userRepo.save(user)

        //when
        val foundUser = userRepo.findByUserId(user.userId)

        //then
        Assertions.assertThat(foundUser?.userId).isEqualTo(user.userId)
    }

    @Test
    fun itShouldCheckSearchUserByUserIdNotFound() {
        //given

        //when
        val foundUser = userRepo.findByUserId("dsfsdfsdfeweqcvcxv")

        //then
        Assertions.assertThat(foundUser).isNull()
    }

    private fun createUser(nameUser: String, role: EnumRole): User {
        return User(
            null,
            userId = UUID.randomUUID().toString().replace("-", ""),
            firstName = nameUser,
            lastName = "Иванова",
            username = nameUser,
            password = "1234",
            email = "${nameUser}@mail.ru",
            "",
            null,
            null,
            joinDate = Date(),
            isActive = true,
            isNotLocked = true,
            role = role.name,
            authorities = role.authorities.toMutableList())
    }
}