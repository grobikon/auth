#FROM gradle:7.2-jdk11 AS build
#COPY --chown=gradle:gradle . /home/gradle/src
#WORKDIR /home/gradle/src
#RUN gradle build

FROM openjdk:11
EXPOSE 5000
ARG JAR_FILE=build/libs/auth-0.0.1.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
#ADD build/libs/*.jar app.jar
#ENV APP_HOME=/usr/app
#WORKDIR $APP_HOME
#COPY ./build/libs/* ./app.jar
#CMD ["java", "-jar","app.jar"]